# Estampatodo

Estampador de pdfs

## Requisitos

* OpenJDK 13
* Docker
* Gradble

## Generacion de la imagen

Con java y gradle en path:

```
./gradlew jibBuildTar
```

Esto genera el archivo estampatodo.tar. Este es un tar con la imagen.

Cargamos la imagen:

```
docker load < estampatodo.tar
```

Para ejecutarlo:

```
docker run -p 8080:8080 -v $(pwd)/config:/app/classes/config:ro necro.nomi.com.ar/estampatodo:latest
```
