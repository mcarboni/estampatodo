#!/bin/bash

# hago una copia en nombre.pdf -> nombre0.pdf
aux1=$(printf "%s%s.pdf" ${1%%.*} 0)
cp $1 $aux1


for i in $(seq 1 $2); do
  SHOW_LOGO=false #true
  # armo el nombre con el elemento i
  aux2=$(printf "%s%s.pdf" ${1%%.*} $i)
  curl -X POST http://localhost:8080/stamp --data-binary "@$aux1" \
       -H "Content-type: application/pdf" \
       -H "X-Document-Stamp-Reason: Porque pintó" \
       -H "X-Document-Stamp-Show-Logo: $SHOW_LOGO" \
       -H "X-Document-Password: nolocuentes" \
       -H "X-Document-Stamp-posY:650.0" \
       > "$aux2"
#       -H "X-Document-Stamp-posX:10.0" \
  # borro el anterior
  rm $aux1
  aux1=$aux2
done
