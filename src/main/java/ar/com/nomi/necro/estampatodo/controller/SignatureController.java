package ar.com.nomi.necro.estampatodo.controller;

import ar.com.nomi.necro.estampatodo.service.SignService;
import ar.com.nomi.necro.estampatodo.service.StamperService;
import org.apache.pdfbox.io.IOUtils;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.tsp.TSPException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import java.io.*;
import java.security.GeneralSecurityException;
import java.util.Map;

@RestController
public class SignatureController {

        @Autowired
        private SignService signService;

        @PostMapping(path = "/signatures",  consumes = "application/pdf;charset=UTF-8", produces = "application/pdf;charset=UTF-8")
        public ResponseEntity<Object> stamp(InputStream inputStream,
                                              @RequestHeader(value = "X-Document-Password", required = false) String password) {
            File file = null;
            try {
                file = File.createTempFile("veryfing", "tmp");
                FileOutputStream out = new FileOutputStream(file);
                IOUtils.copy(inputStream, out);
                HttpHeaders headers = new HttpHeaders();
                Map<String, Object> signatures = signService.obtenerFirmas(file, password);

                return ResponseEntity.ok()
                        .headers(headers)
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(signatures);
            } catch (IOException | GeneralSecurityException | CMSException | TSPException | OperatorCreationException e) {
                e.printStackTrace();
                throw new RuntimeException(e);
            } finally {
                if (file != null && file.exists()) {
                    file.delete();
                }
            }


        }
}
