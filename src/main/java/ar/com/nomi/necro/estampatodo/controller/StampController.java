package ar.com.nomi.necro.estampatodo.controller;

import ar.com.nomi.necro.estampatodo.service.StamperService;
import org.apache.pdfbox.io.IOUtils;
import org.apache.pdfbox.pdmodel.font.PDTrueTypeFont;
import org.apache.pdfbox.pdmodel.font.PDType1CFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import static java.lang.Boolean.TRUE;

@RestController
public class StampController {

    @Autowired
    private StamperService stamperService;

    @GetMapping("/")
    public String aiuda() {
        return "Estampa todo:\nUso: curl -X POST http://localhost:8080/stamp --data-binary \"@archivo.pdf\" -H \"Content-type: application/pdf\"\b";
    }

    @PostMapping(path = "/stamp",  consumes = "application/pdf;charset=UTF-8", produces = "application/pdf;charset=UTF-8")
    public ResponseEntity<Resource> stamp(InputStream inputStream,
                                          @RequestHeader("Content-length") long length,
                                          @RequestHeader(value = "X-Document-Password", required = false) String password,
                                          @RequestHeader(value = "X-Document-Write-Protection-Password", required = false) String writeProtectionPassword,
                                          @RequestHeader(value = "X-Document-Stamp-Reason", required = false) String reason,
                                          @RequestHeader(value = "X-Document-Stamp-Show-Logo", required = false) String showLogo,
                                          @RequestHeader(value = "X-Document-Stamp-posX", required = false) Float posX,
                                          @RequestHeader(value = "X-Document-Stamp-posY", required = false) Float posY,
                                          @RequestHeader(value = "X-Document-Stamp-Page-Label", required = false, defaultValue = "STAMPS") String stampPageLabel,
                                          @RequestHeader(value = "X-Document-Stamp-Field-Name", required = false) String fieldName) {
        try {

            File file = File.createTempFile("stamping", "tmp");
            FileOutputStream out = new FileOutputStream(file);
            IOUtils.copy(inputStream, out);


            if (reason != null) {
                reason = new String(reason.getBytes("ISO-8859-1"), "UTF-8");
            }

            InputStream data = stamperService.createJob()
                    .file(file)
                    .password(password)
                    .label(stampPageLabel)
                    .fieldName(fieldName)
                    .reason(reason)
                    .posX(posX)
                    .posY(posY)
                    .showLogo(showLogo != null ? "TRUE".equals(showLogo.strip().toUpperCase()) : null)
                    .stampedStream();
            file.delete();

            InputStreamResource resource = new InputStreamResource(data);
            HttpHeaders headers = new HttpHeaders();
            return ResponseEntity.ok()
                    .headers(headers)
//                    .contentLength(length)
                    .contentType(MediaType.parseMediaType("application/octet-stream"))
                    .body(resource);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }
}
