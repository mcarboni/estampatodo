package ar.com.nomi.necro.estampatodo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.InputStream;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;

@Service
@ConfigurationProperties(prefix="keystore")
public class KeystoreService {

    @Autowired
    private ResourceManager resourceManager;

    private static final String PROVIDER_BC = "BC";
    private String type;
    private String location;

    private String password;
    private String alias;

    private PrivateKey privateKey;
    private Certificate[] chain;

    @PostConstruct
    public void init(){
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());

        KeyStore keystore = null;
        try {
            keystore = KeyStore.getInstance(type, PROVIDER_BC);

            InputStream input = resourceManager.getResourceAsStream(location);

            keystore.load(input, password.toCharArray());

            this.privateKey = (PrivateKey) keystore.getKey(alias, password.toCharArray());

            this.chain = keystore.getCertificateChain(alias);
            if (chain == null || chain.length == 0)
                throw new RuntimeException("No valen certificados aufirmados, zonzo.");

        } catch (KeyStoreException | NoSuchProviderException | CertificateException
                | UnrecoverableKeyException | NoSuchAlgorithmException | IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }
    public PrivateKey getPrivateKey() {
        return privateKey;
    }

    public Certificate[] getChain() {
        return chain;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }
}
