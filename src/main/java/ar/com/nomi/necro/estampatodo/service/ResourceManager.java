package ar.com.nomi.necro.estampatodo.service;

import org.slf4j.Logger;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import static org.slf4j.LoggerFactory.getLogger;

@Service
@ConfigurationProperties(prefix="resource")
public class ResourceManager {

    private static Logger logger = getLogger(ResourceManager.class.getName());

    private static final String CLASSPATH = "classpath";

    private String protocol = "classpath";

    private String base = "//";

    public void setBase(String base) {
        this.base = base;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public URL getURL(String resource) throws MalformedURLException {
        if (CLASSPATH.equals(protocol)) {
            return ClassLoader.getSystemResource(String.format("%s", resource));
        }
        resource = String.format("%s:%s/%s", protocol, base, resource);
        logger.info(resource);
        return new URL(resource);
    }

    public InputStream getResourceAsStream(String resource) throws IOException {
        return getURL(resource).openStream();
    }

}
