package ar.com.nomi.necro.estampatodo.service;

import ar.com.nomi.necro.estampatodo.signing.BaseSigner;
import ar.com.nomi.necro.estampatodo.signing.VisibleSigner;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSString;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.interactive.digitalsignature.PDSignature;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.cms.*;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.tsp.TSPException;
import org.bouncycastle.tsp.TimeStampToken;
import org.bouncycastle.util.Selector;
import org.bouncycastle.util.Store;
import org.bouncycastle.util.StoreException;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Service;

import javax.naming.InvalidNameException;
import javax.naming.ldap.LdapName;
import javax.naming.ldap.Rdn;
import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.io.*;
import java.security.GeneralSecurityException;
import java.security.MessageDigest;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.*;
import java.util.List;

import static org.slf4j.LoggerFactory.getLogger;

@Service
@ConfigurationProperties(prefix="signature")
public class SignService {

    @Autowired
    private StamperService stamperService;

    private Logger logger = getLogger(SignService.class.getName());

    @Autowired
    private KeystoreService keystoreService;

    @Autowired
    private ResourceManager resourceManager;

    private String imagePath;

    private String bgcolor;

    private String system;

    private static final int SIGNATURE_SIZE = 16000;

    private int porPagina = 32;

    private int porFila = 4;

    private float x0 = 10.0f;

    private float y0 = 100.0f;

    private float ancho = 125;

    private float altura = 50;

    private float dx = 10;

    private float dy = 5;

    private Boolean showLogo;
    private String systemLabel;

    public void sign(File file, File signedFile, String fieldName, int pageNumber, int signNumber, String reason, Boolean requestShowLogo) throws IOException {
        int pagina = signNumber / porPagina;
        int nroFirma = signNumber % porPagina;
        int nroFila = nroFirma / porFila;
        int nroColumna = nroFirma % porFila;
        fieldName = String.format("%s_%d", fieldName, signNumber);

        if (pagina == 0) {
            VisibleSigner visibleSigner = new VisibleSigner();
            visibleSigner.setReason(reason);
            visibleSigner.setPrivateKey(keystoreService.getPrivateKey());
            visibleSigner.setCertificateChain(keystoreService.getChain());
            visibleSigner.setImageFile(new File(resourceManager.getURL(imagePath).getFile()));
            visibleSigner.setBgcolor(Color.decode(bgcolor));
            visibleSigner.setExternalSigning(true);
            visibleSigner.setSystemLabel(systemLabel);
            visibleSigner.setSystem(system);
            visibleSigner.setShowLogo( requestShowLogo == null ? this.showLogo : requestShowLogo  );
            Rectangle2D humanRect = new Rectangle2D.Float(x0 + (ancho+dx) * nroColumna, y0 + (altura + dy) * nroFila, ancho, altura);
            visibleSigner.signPDF(file, signedFile, humanRect, fieldName, pageNumber);
        } else {
            BaseSigner invisibleSigner = new BaseSigner();
            invisibleSigner.setReason(reason);
            invisibleSigner.setPrivateKey(keystoreService.getPrivateKey());
            invisibleSigner.setCertificateChain(keystoreService.getChain());
            invisibleSigner.setExternalSigning(true);
            invisibleSigner.signPDF(file, signedFile, fieldName, pageNumber);
        }
    }

    public void signAt(File file, File signedFile, String fieldName, int pageNumber, String reason, Boolean requestShowLogo, Float posX, Float posY) throws IOException {
        if (pageNumber > 0) {
            VisibleSigner visibleSigner = new VisibleSigner();
            visibleSigner.setPrivateKey(keystoreService.getPrivateKey());
            visibleSigner.setCertificateChain(keystoreService.getChain());
            visibleSigner.setImageFile(new File(resourceManager.getURL(imagePath).getFile()));
            visibleSigner.setBgcolor(Color.decode(bgcolor));
            visibleSigner.setExternalSigning(true);
            visibleSigner.setReason(reason);
            logger.info(this.showLogo != null ? this.showLogo.toString(): "n/d");
            visibleSigner.setShowLogo( requestShowLogo == null ? this.showLogo : requestShowLogo  );
            visibleSigner.setSystemLabel(systemLabel);
            visibleSigner.setSystem(system);
            Rectangle2D humanRect = new Rectangle2D.Float( posX != null ? posX : x0  ,  posY != null ? posY : y0 + 600.0f , ancho, altura);
            visibleSigner.signPDF(file, signedFile, humanRect, fieldName, pageNumber);
        } else {
            BaseSigner invisibleSigner = new BaseSigner();
            invisibleSigner.setReason(reason);
            invisibleSigner.setPrivateKey(keystoreService.getPrivateKey());
            invisibleSigner.setCertificateChain(keystoreService.getChain());
            invisibleSigner.setExternalSigning(true);
            invisibleSigner.signPDF(file, signedFile, fieldName, pageNumber);
        }
    }

    private InputStream getImageStream() throws IOException {
        return resourceManager.getResourceAsStream(imagePath);
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public void setBgcolor(String bgcolor) {
        this.bgcolor = bgcolor != null ? bgcolor.toLowerCase() : null;
    }

    public void setPorPagina(int porPagina) {
        this.porPagina = porPagina;
    }

    public void setPorFila(int porFila) {
        this.porFila = porFila;
    }

    public void setX0(float x0) {
        this.x0 = x0;
    }

    public void setY0(float y0) {
        this.y0 = y0;
    }

    public void setDx(float dx) {
        this.dx = dx;
    }

    public void setDy(float dy) {
        this.dy = dy;
    }

    public void setAltura(float altura) {
        this.altura = altura;
    }

    public void setAncho(float ancho) {
        this.ancho = ancho;
    }

    public void setSystem(String system) {
        this.system = system;
    }

    public void setShowLogo(Boolean showLogo) {
        this.showLogo = showLogo;
    }

    public void setSystemLabel(String systemLabel) {
        this.systemLabel = systemLabel;
    }


    public Map<String, Object> obtenerFirmas(File file, String password) throws IOException, GeneralSecurityException, CMSException, TSPException, OperatorCreationException {
        Map<String, Object> data = new LinkedHashMap();
        List<Map<String, Object>> signatures = new LinkedList();

        PDDocument document = PDDocument.load(file, password);
        registerIfNotNull(data,"numberOfPages", document.getNumberOfPages());
        registerIfNotNull(data,"isEncrypted", document.isEncrypted());
        registerIfNotNull(data,"title", document.getDocumentInformation().getTitle());
        registerIfNotNull(data,"subject", document.getDocumentInformation().getSubject());
        registerIfNotNull(data,"modificationDate", document.getDocumentInformation().getModificationDate());
//        registerIfNotNull(data, "checksum", document.);

        for (PDSignature sig : document.getSignatureDictionaries())
        {
            byte[] buf = null;
            try (FileInputStream fis = new FileInputStream(file))
            {
                buf = sig.getSignedContent(fis);
            }

            Map<String, Object> signData = new LinkedHashMap();
            registerIfNotNull(signData, "signedDate", sig.getSignDate());
            registerIfNotNull(signData,"contactInfo", sig.getContactInfo());
            registerIfNotNull(signData,"name", sig.getName());
            registerIfNotNull(signData,"reason", sig.getReason());
            registerIfNotNull(signData,"location", sig.getLocation());

            handleCerts(signData,sig,buf);

            signatures.add(signData);
        }
        data.put("signatures", signatures);
        return data;
    }

    private void handleCerts(Map<String, Object> data, PDSignature signature, byte[] buf) throws GeneralSecurityException, CMSException, IOException, TSPException, OperatorCreationException {
        COSDictionary sigDict = signature.getCOSObject();
        COSString contents = (COSString) sigDict.getDictionaryObject(COSName.CONTENTS);

        String subFilter = signature.getSubFilter();
        switch (subFilter)
        {
            case "adbe.pkcs7.detached":
            case "ETSI.CAdES.detached":
                verifyPKCS7(data, buf, contents, signature);

                break;
            case "adbe.pkcs7.sha1":
            {
                byte[] certData = contents.getBytes();
                CertificateFactory factory = CertificateFactory.getInstance("X.509");
                ByteArrayInputStream certStream = new ByteArrayInputStream(certData);
                Collection<? extends Certificate> certs = factory.generateCertificates(certStream);
                data.put("certs", certs);
                verifyPKCS7(data, buf, contents, signature);

                break;
            }
            case "adbe.x509.rsa_sha1":
            {
                COSString certString = (COSString) sigDict.getDictionaryObject(COSName.CERT);
                if (certString == null)
                {
                    System.err.println("The /Cert certificate string is missing in the signature dictionary");
                    data.put("error","The /Cert certificate string is missing in the signature dictionary");
                }
                byte[] certData = certString.getBytes();
                CertificateFactory factory = CertificateFactory.getInstance("X.509");
                ByteArrayInputStream certStream = new ByteArrayInputStream(certData);
                Collection<? extends Certificate> certs = factory.generateCertificates(certStream);
                data.put("certs", certs);

                break;
            }
            case "ETSI.RFC3161":
                TimeStampToken timeStampToken = new TimeStampToken(new CMSSignedData(contents.getBytes()));
                data.put("timeStampGenTime", timeStampToken.getTimeStampInfo().getGenTime());
                data.put("timeStampTsaName", timeStampToken.getTimeStampInfo().getTsa().getName());

                CertificateFactory factory = CertificateFactory.getInstance("X.509");
                ByteArrayInputStream certStream = new ByteArrayInputStream(contents.getBytes());
                Collection<? extends Certificate> certs = factory.generateCertificates(certStream);
                data.put("certs", certs);

                break;

            default:
                data.put("error", "Unknown certificate type: " + subFilter);
                break;
        }
    }

    /**
     * Verify a PKCS7 signature.
     *
     * @param byteArray the byte sequence that has been signed
     * @param contents the /Contents field as a COSString
     * @param sig the PDF signature (the /V dictionary)
     * @throws CertificateException
     * @throws CMSException
     * @throws StoreException
     * @throws OperatorCreationException
     */
    private void verifyPKCS7(Map<String, Object> data, byte[] byteArray, COSString contents, PDSignature sig)
            throws CMSException, GeneralSecurityException, StoreException, OperatorCreationException {
        // inspiration:
        // http://stackoverflow.com/a/26702631/535646
        // http://stackoverflow.com/a/9261365/535646

        CMSProcessable signedContent = new CMSProcessableByteArray(byteArray);
        CMSSignedData signedData = new CMSSignedData(signedContent, contents.getBytes());
        Store<X509CertificateHolder> certificatesStore = signedData.getCertificates();
        Collection<SignerInformation> signers = signedData.getSignerInfos().getSigners();
        SignerInformation signerInformation = signers.iterator().next();
        @SuppressWarnings("unchecked")
        Collection<X509CertificateHolder> matches =
                certificatesStore.getMatches((Selector<X509CertificateHolder>) signerInformation.getSID());
        X509CertificateHolder certificateHolder = matches.iterator().next();
        X509Certificate certFromSignedData = new JcaX509CertificateConverter().getCertificate(certificateHolder);
        // data.put("certFromSignedData", certFromSignedData.toString());

        try {
            LdapName ln = new LdapName(certFromSignedData.getSubjectX500Principal().getName());
            for(Rdn rdn : ln.getRdns()) {
                if (rdn.getType().equalsIgnoreCase("CN")) {
                    data.putIfAbsent("name", rdn.getValue());
                } else if (rdn.getType().equalsIgnoreCase("T")) {
                    data.putIfAbsent("title", rdn.getValue());
                } else if (rdn.getType().equalsIgnoreCase("TITLE")) {
                    data.putIfAbsent("title", rdn.getValue());
                } else if (rdn.getType().equalsIgnoreCase("O")) {
                    data.putIfAbsent("organizationName", rdn.getValue());
                } else if (rdn.getType().equalsIgnoreCase("OU")) {
                    data.putIfAbsent("organizationUnit", rdn.getValue());
                } else if (rdn.getType().equalsIgnoreCase("SERIALNUMBER")) {
                    data.putIfAbsent("serialNumber", rdn.getValue());
                } else if (rdn.getType().equalsIgnoreCase("L")) {
                    data.putIfAbsent("location", rdn.getValue());
                }

            }
        } catch (InvalidNameException e) {
            e.printStackTrace();
        }

        certFromSignedData.checkValidity(sig.getSignDate().getTime());
    }

    private void registerIfNotNull(Map<String, Object> data, String key, Object value) {
        if (value != null)
            data.put(key, value);
    }
}
