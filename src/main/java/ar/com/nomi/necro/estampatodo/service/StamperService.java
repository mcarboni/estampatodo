package ar.com.nomi.necro.estampatodo.service;

import org.apache.pdfbox.io.IOUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDPageLabelRange;
import org.apache.pdfbox.pdmodel.common.PDPageLabels;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static org.slf4j.LoggerFactory.getLogger;

@Service
@ConfigurationProperties(prefix="stamper")
public class StamperService {

    public class StampJob {

        private File file;
        private String password;
        private String label;
        private String fieldName;
        private String reason;
        private Boolean showLogo;
        private Float posX;
        private Float posY;

        public StampJob file(File file) {
            this.file = file;
            return this;
        }


        public StampJob password(String password) {
            this.password = password;
            return this;
        }

        public StampJob label(String label) {
            this.label = label;
            return this;
        }

        public StampJob fieldName(String fieldName) {
            this.fieldName = fieldName;
            return this;
        }

        public StampJob reason(String reason) {
            this.reason = reason;
            return this;
        }

        public StampJob posX(Float posX) {
            this.posX = posX;
            return this;
        }

        public StampJob posY(Float posY) {
            this.posY = posY;
            return this;
        }

        public InputStream stampedStream() throws IOException {
            Map<String, Object> data = new LinkedHashMap<>();
            PDDocument document = null;
            File extraPageFile = File.createTempFile("extraPage", "tmp");
            File signedFile = File.createTempFile("signing", "tmp");

            try {
                File theFile;
                document = PDDocument.load(file, password);
                logger.debug("isReadOnly? " + document.getCurrentAccessPermission().isReadOnly());
                if (document.isEncrypted() && password == null) {
                    throw new RuntimeException("El documento está protegido y no se ha provisto el password correcto.");
                }
                //document.getEncryption().get;
                List<Map<String, Object>> signatures = new LinkedList<>();
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                boolean signed = !document.getSignatureDictionaries().isEmpty();
/*
            for (PDSignature sig : document.getSignatureDictionaries())
            {
                logger.info(sig.toString());
            }
            data.put("signatures", signatures);
*/

                theFile = file;
                int estampas = 0;
                PDPage paginaDeFirmas = getStampPage(document, label);
                if (paginaDeFirmas == null) {
                    if (!signed) {
                        paginaDeFirmas = createPage(document, label);
                        document.save(new FileOutputStream(extraPageFile));
                        theFile = extraPageFile;
                    }
                } else {
                    estampas = paginaDeFirmas.getAnnotations().size();
                }

                int pageNumber = document.getNumberOfPages();

                if (paginaDeFirmas != null) {
                    signService.sign(theFile, signedFile, fieldName, pageNumber, estampas, reason, showLogo);
                } else {
                    pageNumber = -1;
                    signService.signAt(theFile, signedFile, fieldName, pageNumber, reason, showLogo, posX, posY);
                }


                FileInputStream out2 = new FileInputStream(signedFile);

                return new ByteArrayInputStream(out2.readAllBytes());
            } catch (IOException ex) {
                throw new IOException(ex);
            } finally {
                IOUtils.closeQuietly(document);
                signedFile.delete();
            }
        }

        public StampJob showLogo(Boolean showLogo) {
            this.showLogo = showLogo;
            return this;
        }
    }

    private Logger logger = getLogger(StamperService.class.getName());

    @Autowired
    private SignService signService;

    public StampJob createJob() {
        return new StampJob();
    }

    private String title;

    public InputStream stamp(File file, String password, String stampPageLabel, String fieldName) throws IOException {
        return createJob().file(file).password(password).label(stampPageLabel).fieldName(fieldName).stampedStream();
    }

    public PDPage getStampPage(PDDocument doc, String stampPageLabel) throws IOException {

        PDPageLabels pageLabels = doc.getDocumentCatalog().getPageLabels();
        if (pageLabels == null) {
            pageLabels = new PDPageLabels(doc);
        }

        String prefix = stampPageLabel+" ";
        for (Integer i : pageLabels.getPageIndices()) {
            PDPageLabelRange range = pageLabels.getPageLabelRange(i);

            if ( range.getPrefix() != null && range.getPrefix().startsWith(stampPageLabel)  ) {
                return doc.getPage(range.getStart());
            }
        }

        return null;
    }

    private PDPage createPage(PDDocument doc, String stampPageLabel) {
        int page = doc.getNumberOfPages();
        PDPage laPage = new PDPage(PDRectangle.A4);
        doc.addPage(laPage);
        PDPageLabels pageLabels = new PDPageLabels(doc);


        try {
            PDPageContentStream stream = new PDPageContentStream(doc, laPage);
            PDFont font = PDType1Font.HELVETICA_BOLD; // Or whatever font you want.

            int marginTop = 30;
            int fontSize = 16; // Or whatever font size you want.
            float titleWidth = font.getStringWidth(title) / 1000 * fontSize;
            float titleHeight = font.getFontDescriptor().getFontBoundingBox().getHeight() / 1000 * fontSize;

            float startX = (laPage.getMediaBox().getWidth() - titleWidth) / 2;
            float startY = laPage.getMediaBox().getHeight() - marginTop - titleHeight;

            stream.beginText();
            stream.setFont(font, fontSize);
            stream.newLineAtOffset(startX, startY);
            stream.showText(title);
            stream.endText();
            stream.close();

            stream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        PDPageLabelRange pageLabelRangeLetters = new PDPageLabelRange();
        pageLabelRangeLetters.setStyle(PDPageLabelRange.STYLE_DECIMAL);
        pageLabelRangeLetters.setPrefix(stampPageLabel+" ");
        pageLabelRangeLetters.setStart(1);
        pageLabels.setLabelItem(page, pageLabelRangeLetters);
        doc.getDocumentCatalog().setPageLabels(pageLabels);

        return laPage;
    }

    private boolean isStampPage(PDPage p) throws IOException {
        return false;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
